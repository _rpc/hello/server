import 'package:grpc/grpc.dart' as grpc;
import 'package:server/src/hello/hello_service.dart';

class AppServer {
  Future<void> main(int port) async {
    final server = grpc.Server([HelloService()]);
    await server.serve(port: port);
    print('Server is listening on : $port');
  }
}
