///
//  Generated code. Do not modify.
//  source: hello_service.proto
//
// @dart = 2.3
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'hello_service.pb.dart' as $0;
export 'hello_service.pb.dart';

class HelloServiceClient extends $grpc.Client {
  static final _$greet = $grpc.ClientMethod<$0.Greeted, $0.Greeting>(
      '/helloService.HelloService/Greet',
      ($0.Greeted value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Greeting.fromBuffer(value));
  static final _$randomGreetings = $grpc.ClientMethod<$0.Void, $0.Greeting>(
      '/helloService.HelloService/RandomGreetings',
      ($0.Void value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Greeting.fromBuffer(value));
  static final _$greetAllAtOnce = $grpc.ClientMethod<$0.Greeted, $0.Greeting>(
      '/helloService.HelloService/GreetAllAtOnce',
      ($0.Greeted value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Greeting.fromBuffer(value));
  static final _$greetOneByOne = $grpc.ClientMethod<$0.Greeted, $0.Greeting>(
      '/helloService.HelloService/GreetOneByOne',
      ($0.Greeted value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Greeting.fromBuffer(value));

  HelloServiceClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.Greeting> greet($0.Greeted request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$greet, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.Greeting> randomGreetings($0.Void request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$randomGreetings, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.Greeting> greetAllAtOnce(
      $async.Stream<$0.Greeted> request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$greetAllAtOnce, request, options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.Greeting> greetOneByOne(
      $async.Stream<$0.Greeted> request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$greetOneByOne, request, options: options);
    return $grpc.ResponseStream(call);
  }
}

abstract class HelloServiceBase extends $grpc.Service {
  $core.String get $name => 'helloService.HelloService';

  HelloServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Greeted, $0.Greeting>(
        'Greet',
        greet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Greeted.fromBuffer(value),
        ($0.Greeting value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Void, $0.Greeting>(
        'RandomGreetings',
        randomGreetings_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.Void.fromBuffer(value),
        ($0.Greeting value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Greeted, $0.Greeting>(
        'GreetAllAtOnce',
        greetAllAtOnce,
        true,
        false,
        ($core.List<$core.int> value) => $0.Greeted.fromBuffer(value),
        ($0.Greeting value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Greeted, $0.Greeting>(
        'GreetOneByOne',
        greetOneByOne,
        true,
        true,
        ($core.List<$core.int> value) => $0.Greeted.fromBuffer(value),
        ($0.Greeting value) => value.writeToBuffer()));
  }

  $async.Future<$0.Greeting> greet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Greeted> request) async {
    return greet(call, await request);
  }

  $async.Stream<$0.Greeting> randomGreetings_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Void> request) async* {
    yield* randomGreetings(call, await request);
  }

  $async.Future<$0.Greeting> greet($grpc.ServiceCall call, $0.Greeted request);
  $async.Stream<$0.Greeting> randomGreetings(
      $grpc.ServiceCall call, $0.Void request);
  $async.Future<$0.Greeting> greetAllAtOnce(
      $grpc.ServiceCall call, $async.Stream<$0.Greeted> request);
  $async.Stream<$0.Greeting> greetOneByOne(
      $grpc.ServiceCall call, $async.Stream<$0.Greeted> request);
}
