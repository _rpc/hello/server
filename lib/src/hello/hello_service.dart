import 'dart:async';
import 'package:grpc/grpc.dart';
import 'package:server/src/generated/hello_service.pbgrpc.dart';
import 'package:faker/faker.dart';

class HelloService extends HelloServiceBase {
  @override
  Future<Greeting> greet(ServiceCall call, Greeted request) async {
    print('RPC_GREET: Hello ${request.name}');
    return Future.value(Greeting()..message = 'Hello ${request.name}');
  }

  @override
  Future<Greeting> greetAllAtOnce(
      ServiceCall call, Stream<Greeted> request) async {
    var heroes = await request.map((event) {
      print(event);
      return event.name;
    }).reduce((previous, element) => '$previous, $element');
    var answer = 'Hello: $heroes!!';
    print(answer);
    return Greeting()..message = answer;
  }

  @override
  Stream<Greeting> greetOneByOne(
      ServiceCall call, Stream<Greeted> request) async* {
    await for (Greeted element in request) {
      print(element);
      yield await Future.delayed(Duration(seconds: 1),
          () => Greeting()..message = 'Hello ${element.name}');
    }
  }

  @override
  Stream<Greeting> randomGreetings(ServiceCall call, Void request) async* {
    for (var _ in List.generate(10, (i) => i)) {
      yield await Future.delayed(Duration(seconds: 2),
          () => Greeting()..message = 'Hello ${Faker().person.firstName()}');
    }
    return;
  }
}
