import 'package:server/app_server.dart';

const port = int.fromEnvironment('PORT', defaultValue: 8085);
void main(List<String> arguments) async {
  var appServer = AppServer();
  await appServer.main(port);
}
